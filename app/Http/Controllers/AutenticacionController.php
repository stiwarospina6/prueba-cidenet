<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Validator;

class AutenticacionController extends Controller
{
    public $Respuesta = ['data' => [], 'mensaje' => [], 'error' => 1];

    public function login()
    {
        if (session()->has('id_usuario')) {
            return redirect('platform/dashboard');
        }

        return view('login');
    }

    public function validar(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'usuario' => 'required',
            'clave' => 'required'
        ]);

        if ($validator->fails()) {
            $this->Respuesta['mensaje'] = $validator->messages()->all();
        }

        if (empty($this->Respuesta['mensaje'])) {
            $datos = $request->all();

            $usuarioValidado = User::firstWhere(["email" => $datos["usuario"]]);

            if (($usuarioValidado && Hash::check($datos["clave"], $usuarioValidado->password))) {
                session(['id_usuario' => $usuarioValidado->id]);
                session(['usuario' => $usuarioValidado->email]);
                session(['name' => $usuarioValidado->name]);

                $this->Respuesta['data'] = $usuarioValidado;
                $this->Respuesta['error'] = 0;
            } else {
                $this->Respuesta['mensaje'][] = "Usuario y/o contraseña incorrecta";
            }
        }

        return response()->json($this->Respuesta, 200);
    }

    public function logout()
    {
        Auth::logout();
        //OLVIDAR VARIABLES DE LA SESSION
        Session::flush();

        return redirect()->intended('/')->with('status', 'Gracias por su visita!');
    }
}
