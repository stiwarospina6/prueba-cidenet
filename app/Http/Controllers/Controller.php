<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $respuesta = ["data" => [], "mensaje" => [], "error" => 1];

    public function respuestaNoError()
    {
        $this->respuesta["error"] = 0;
    }

    //registra un nuevo mensaje de error.
    public function agregarError($mensaje)
    {
        $this->respuesta["mensaje"][] = $mensaje;
    }

    //verifica si hay mensajes de error
    public function existeError()
    {
        if (empty($this->respuesta["mensaje"])) {
            return false;
        } else {
            return true;
        }
    }

    public function enviarRespuesta($codeHttp = 200)
    {
        return response()->json($this->respuesta, (int) $codeHttp);
    }
}
