<?php

namespace App\Http\Controllers;

use App\Models\Empleado;
use Illuminate\Http\Request;

class MasterController extends Controller
{
    public function dashboard()
    {
        $empleados = Empleado::get();
        $cantidadEmpleados = $empleados->count();
        $empleadosActivos = Empleado::where("estado", "activo")->count();
        $empleadosInactivos = Empleado::where("estado", "inactivo")->count();

        return view('platform.dashboard', compact("empleados", "cantidadEmpleados", "empleadosActivos", "empleadosInactivos"));
    }
}
