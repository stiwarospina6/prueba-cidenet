<?php

namespace App\Http\Controllers;

use App\Models\Empleado;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Validator;

class EmpleadosController extends Controller
{
    public function index()
    {
        $empleados = Empleado::orderBy("fecha_registro", "DESC")->get();

        return view("platform.pages.empleados.index", compact("empleados"));
    }

    public function create()
    {
        return view("platform.pages.empleados.create");
    }

    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                "primer_nombre" => "required|max:20",
                "otros_nombres" => "max:50",
                "primer_apellido" => "required|max:20",
                "segundo_apellido" => "required|max:20",
                "pais_empleo" => "required",
                "area" => "required",
                "correo" => "required|max:300|email",
                "tipo_documento" => "required",
                "identificacion" => "required|max:20",
                "fecha_registro" => "required"
            ]);

            if ($validator->fails()) {
                $this->Respuesta['mensaje'] = $validator->messages()->all();
            }

            if (empty($this->Respuesta['mensaje'])) {
                $datos = $request->all();

                //verificamos que el tipo de documento y numero no se encuentren registrados
                $validarDocumento = Empleado::where("tipo_documento", $datos['tipo_documento'])->where("identificacion", $datos['identificacion'])->first();

                if (!empty($validarDocumento)) {
                    $this->agregarError("El numero de documento y tipo ya se encuentra registrados");
                }

                if (!$this->existeError()) {
                    $empleado = Empleado::create($datos);

                    if ($empleado) {
                        $this->respuestaNoError();
                    }
                }
            }
        } catch (Exception $ex) {
            Log::debug('Fallo la creacion del empleado' . $ex->getMessage());
        }

        return $this->enviarRespuesta();
    }

    public function edit($id)
    {
        $empleado = Empleado::find($id);

        return view("platform.pages.empleados.edit", compact('empleado'));
    }

    public function update(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                "primer_nombre" => "required|max:20",
                "otros_nombres" => "max:50",
                "primer_apellido" => "required|max:20",
                "segundo_apellido" => "required|max:20",
                "pais_empleo" => "required",
                "area" => "required",
                "correo" => "required|max:300|email",
                "tipo_documento" => "required",
                "identificacion" => "required|max:20"
            ]);

            if ($validator->fails()) {
                $this->Respuesta['mensaje'] = $validator->messages()->all();
            }

            if (empty($this->Respuesta['mensaje'])) {
                $datos = $request->all();

                //verificamos que el tipo de documento y numero no se encuentren registrados
                $validarDocumento = Empleado::where("tipo_documento", $datos['tipo_documento'])->where("identificacion", $datos['identificacion'])
                    ->where("id", "!=", $datos['id'])->first();

                if (!empty($validarDocumento)) {
                    $this->agregarError("El numero de documento y tipo ya se encuentra registrados");
                }

                if (!$this->existeError()) {
                    $datos['fecha_actualizacion'] = date('Y-m-d H-m-s');

                    $empleado = Empleado::where('id', $datos['id'])->update($datos);

                    if ($empleado > 0) {
                        $this->respuestaNoError();
                    }
                }
            }
        } catch (Exception $ex) {
            Log::debug('Fallo la modificacion del empleado' . $ex->getMessage());
        }

        return $this->enviarRespuesta();
    }

    public function destroy(Request $request)
    {
        try {
            if (!empty($request->json("identificador"))) {
                $id = $request->json("identificador");

                $empleado = Empleado::find($id);
                $empleado->delete();

                $this->respuestaNoError();
            }
        } catch (Exception $ex) {
            Log::debug('Fallo la eliminacion del empleado' . $ex->getMessage());
        }

        return $this->enviarRespuesta();
    }
}
