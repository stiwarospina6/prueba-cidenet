<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
    use HasFactory;

    protected $table = "empleados";
    const CREATED_AT = null;
    const UPDATED_AT = null;

    protected $fillable = [
        "primer_nombre", "otros_nombres", "primer_apellido", "segundo_apellido", "pais_empleo", "tipo_documento",
        "identificacion", "correo", "area", "fecha_registro", "fecha_actualizacion"
    ];
}
