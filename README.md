# Requerimientos para la instalacion
[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)
- Composer
- Apache
- PHP >= 7.0
- Mysql

# Tecnologias Implementadas
- Axios
- JQuery
- Javascript
- PHP
- Laravel
- Template FlatLab
- Bootstrap

# Instalacion

## Clonacion de repositorio
Lo primero que se debe realizar es la clonacion del repositorio, para realizar esta accion 
debemos ejecutar la siguiente linea de codigo en la terminal de su preferencia, recomendado Git Bash.

Debemos ubicarnos en la ubicacion donde queremos que quede el proyecto. 
Para esto nos moveremos entre carpetas con la terminal de la siguiente manera.
```sh
cd carpeta_donde_quedara_el_proyecto
```

Una vez ubicados en donde queremos que quede el proyecto procederemos a clonarlo
con la siguiente instruccion en nuestra terminal.
```sh
git clone https://gitlab.com/stiwarospina6/prueba-cidenet.git
```

Ya con el repositorio clonado accederemos a el proyecto desde la terminal
y ejecutaremos las siguientes instrucciones.
```sh
cd prueba-cidenet 
composer install
```
A continuacion continuaremos creando el archivo .env una vez el archivo este creado
generaremos la key, para esto escribiremos las siguientes instrucciones en la terminal.
```sh
cp .env.example .env
php artisan key:generate
```
 Ya en este punto el proyecto esta casi completamente configurado.
## Creacion Base de Datos
Iremos a nuestro gestor de base de datos, y crearemos la base de datos con cotejamiento utf8_general_ci
Abriremos el archivo .env y reemplazaremos los datos de nuestra base de datos.

```sh
DB_DATABASE = nombre_base_de_datos
DB_USERNAME = usuario
DB_PASSWORD = clave
```

## Migraciones 
Ya con lo anterior previamente configurado por ultimo nos queda 
ejecutar nuestras migraciones y seeders con la siguiente instruccion, 
este comando lo ejecutaremos en nuestra terminal.
```sh
php artisan migrate:fresh --seed
```

## Ejecucion del programa
Ya habiendo terminado la configuracion el siguiente paso a seguir es poner a correr nuestro programa.
Esto lo haremos de la siguiente manera.

```sh
php artisan serve
```
Ya con esto tenemos nuestra aplicacion en ejecucion, solo nos queda acceder a la ruta generada por el comando anterior.

## Seeder
Al ejecutar el seeder se creo un usuario por defecto para acceder al sistema, el cual es:
correo: admin@cidenet.com.co
clave: admin

# Prueba Cidenet
## Realizado por Stiwar Julian Marin Ospina

