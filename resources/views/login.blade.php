<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Stiwar">
    <meta name="keyword" content="Inventario">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Prueba Cidenet</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('flatlab/css/bootstrap.min.css') }} " rel="stylesheet">
    <link href="{{ asset('flatlab/css/bootstrap-reset.css') }} " rel="stylesheet">
    <!--external css-->
    <link href="{{ asset('flatlab/assets/font-awesome/css/font-awesome.css') }} " rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="{{ asset('flatlab/css/style.css') }} " rel="stylesheet">
    <link href="{{ asset('flatlab/css/style-responsive.css') }} " rel="stylesheet" />

    <script src="{{ asset('flatlab/js/jquery.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

</head>

<body class="login-body">
    <div class="container">
        <form class="form-signin" id="login_form">
            <h2 class="form-signin-heading">Cidenet</h2>
            <div class="login-wrap">
                @csrf
                <input type="text" class="form-control system_validador_vacio system_validador_email" style="margin-bottom: 5px" name="usuario" id="usuario" placeholder="Email" autofocus>
                <input type="password" class="form-control system_validador_vacio" style="margin-bottom: 5px" id="clave" name="clave" placeholder="Clave">
                <button class="btn btn-sm btn-login btn-block mt-3" id="btn_validar" type="button">
                    Iniciar Sesion</button>
            </div>
        </form>
    </div>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="{{ asset('flatlab/js/bootstrap.bundle.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="{{ asset('js/app_request.js') }}"></script>
    <script src="{{ asset('js/validador.js') }}"></script>
    <script>
        $('#clave').keyup(function(e) {
            if (e.keyCode === 13) {
                validarUsuario();
            }
        });

        $('#clave').keyup(function(e) {
            if (e.keyCode === 13) {
                validarUsuario();
            }
        });

        $(document).on('click', '#btn_validar', function() {
            validarUsuario();
        });

        function validarUsuario() {
            if (system_validarcampos("login_form")) {
                axios.post('autenticacion/validar', getDataJson("login_form")).then(function(resp) {
                    if (resp.data.error === 0) {
                        window.location.href = "{{ url('platform/dashboard') }}";
                    } else {
                        notificarUsuario(resp.data.mensaje);
                    }
                });
            }
        }
    </script>
</body>

</html>
