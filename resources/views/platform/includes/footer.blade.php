<footer class="site-footer">
    <div class="text-center">
        {{ date('Y') }} &copy; Cidenet
        <a href="#" class="go-top">
            <i class="fa fa-angle-up"></i>
        </a>
    </div>
</footer>
