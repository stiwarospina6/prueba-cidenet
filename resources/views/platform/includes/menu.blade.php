<?php
$data = extraerMenuJson(); ?>
<aside>
    <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
            @foreach ($data['menu'] as $item)
                <li>
                    <a href="{{ url($item['url']) }}" class="@yield($item['yield'])">
                        <i class="{{ $item['icon'] }}"></i>
                        <span>{{ $item['label'] }}</span>
                    </a>
                </li>
            @endforeach
            <!--multi level menu end-->
        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>
