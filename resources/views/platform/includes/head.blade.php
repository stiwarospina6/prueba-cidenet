<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.png">

    <title> @yield('title') </title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('flatlab/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('flatlab/css/bootstrap-reset.css') }}" rel="stylesheet">
    <!--external css-->
    <link href="{{ asset('flatlab/assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet" />
    <!-- Dinamyc table -->
    <link href="{{ asset('flatlab/assets/advanced-datatable/media/css/demo_page.css') }}" rel="stylesheet" />
    <link href="{{ asset('flatlab/assets/advanced-datatable/media/css/demo_table.css') }}" rel="stylesheet" />
    <link href="{{ asset('flatlab/assets/data-tables/DT_bootstrap.css') }}" rel="stylesheet" />
    <!--right slidebar-->
    <link href="{{ asset('flatlab/css/slidebars.css') }}" rel=" stylesheet">
    <!-- Custom styles for this template -->
    <link href="{{ asset('flatlab/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('flatlab/css/style-responsive.css') }}" rel="stylesheet" />

    <script src="{{ asset('flatlab/js/jquery.js') }}"></script>
    <script src="{{ asset('flatlab/js/bootstrap.bundle.min.js') }}"></script>

    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>
