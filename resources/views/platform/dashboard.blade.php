@extends('platform.layouts.app')
@section('title', 'Panel Principal')
@section('active-dashboard', 'active')

@section('content')
<div class="row state-overview">
    <div class="col-lg-4 col-sm-6">
        <section class="card">
            <div class="symbol terques">
                <i class="fa fa-users"></i>
            </div>
            <div class="value">
                <h1 class="count">
                    {{ $cantidadEmpleados }}
                </h1>
                <p>Empleados Registrados</p>
            </div>
        </section>
    </div>
    <div class="col-lg-4 col-sm-6">
        <section class="card">
            <div class="symbol yellow">
                <i class="fa fa-users"></i>
            </div>
            <div class="value">
                <h1 class=" count2">
                    {{ $empleadosActivos }}
                </h1>
                <p>Empleados Activos</p>
            </div>
        </section>
    </div>
    <div class="col-lg-4 col-sm-6">
        <section class="card">
            <div class="symbol red">
                <i class="fa fa-users"></i>
            </div>
            <div class="value">
                <h1 class=" count3">
                    {{ $empleadosInactivos }}
                </h1>
                <p>Empleados Inactivos</p>
            </div>
        </section>
    </div>
</div>

<!--work progress start-->
<section class="card">
<div class="card-body">
        <div class="adv-table">
            <table class="display table table-bordered table-striped" id="listadoEmpleados">
                <thead>
                    <tr>
                        <th>Primer Nombre</th>
                        <th>Otros Nombres</th>
                        <th>Primer Apellido</th>
                        <th>Segundo Apellido</th>
                        <th>Tipo De Identificacion</th>
                        <th>Identificacion</th>
                        <th>Pais Empleo</th>
                        <th>Correo Electronico</th>
                        <th>Estado</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($empleados as $empleado)
                    <tr class="gradeX">
                        <td> {{ $empleado->primer_nombre }}</td>
                        <td> {{ $empleado->otros_nombres }}</td>
                        <td> {{ $empleado->primer_apellido }}</td>
                        <td> {{ $empleado->segundo_apellido }}</td>
                        <td> {{ $empleado->tipo_documento }}</td>
                        <td> {{ $empleado->identificacion }}</td>
                        <td> {{ $empleado->pais_empleo }}</td>
                        <td> {{ $empleado->correo }}</td>
                        <td>
                            <span class="badge badge-success">
                                {{ mb_strtoupper($empleado->estado) }}
                            </span>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>
<!--work progress end-->
@endsection
