@extends('platform.layouts.app')
@section('title', 'Empleados')
@section('active-empleados', 'active')

@section('content')
<section class="card">
    <header class="card-header">
        Actualizar Empleados
    </header>
    <div class="card-body">
        <form class="form-horizontal tasi-form" id="formEmpleado">
            <input type="hidden" name="id" id="id" value="{{ $empleado->id }}">
            <div class="form-group row">
                <label class="col-sm-2 col-sm-2 control-label">Primer Nombre</label>
                <div class="col-sm-10">
                    <input type="text" value="{{ $empleado->primer_nombre ?? '' }}" name="primer_nombre" id="primer_nombre" class="form-control system_validador_vacio validarInputsText" autocomplete="off" onpaste="return false">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-sm-2 control-label">Otros Nombres</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control validarInputsTextOtherName" value="{{ $empleado->otros_nombres ?? '' }}" name="otros_nombres" id="otros_nombres" autocomplete="off" onpaste="return false">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-sm-2 control-label">Primer Apellido</label>
                <div class="col-sm-10">
                    <input type="text" name="primer_apellido" id="primer_apellido" value="{{ $empleado->primer_apellido ?? '' }}" class="form-control system_validador_vacio validarInputsText" autocomplete="off" onpaste="return false">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-sm-2 control-label">Segundo Apellido</label>
                <div class="col-sm-10">
                    <input type="text" name="segundo_apellido" id="segundo_apellido" value="{{ $empleado->segundo_apellido ?? '' }}" class="form-control system_validador_vacio validarInputsText" autocomplete="off" onpaste="return false">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-sm-2 control-label">Pais Empleo</label>
                <div class="col-sm-10">
                    <select name="pais_empleo" id="pais_empleo" class="form-control system_validador_vacio">
                        <option value="{{ $empleado->pais_empleo ?? ''}}" active>{{ $empleado->pais_empleo ?? 'Seleccione una opcion'}}</option>
                        <option value="colombia">Colombia</option>
                        <option value="estados unidos">Estados Unidos</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-sm-2 control-label">Area</label>
                <div class="col-sm-10">
                    <select name="area" id="area" class="form-control system_validador_vacio">
                        <option value="{{ $empleado->area ?? ''}}">{{ $empleado->area ?? 'Seleccione una opcion'}}</option>
                        <option value="administracion">Administración</option>
                        <option value="financiera">Financiera</option>
                        <option value="compras">Compras</option>
                        <option value="infraestructura">Infraestructura</option>
                        <option value="operacion">Operación</option>
                        <option value="talento humano">Talento Humano</option>
                        <option value="servicios varios">servicios varios</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-sm-2 control-label">Tipo Identificacion</label>
                <div class="col-sm-10">
                    <select name="tipo_documento" id="tipo_documento" class="form-control system_validador_vacio">
                        <option value="{{ $empleado->tipo_documento ?? ''}}">{{ $empleado->tipo_documento ?? 'Seleccione una opcion'}}</option>
                        <option value="cedula ciudadania">Cedula Ciudadanía</option>
                        <option value="cedula extranjeria">Cedula Extranjería</option>
                        <option value="pasaporte">Pasaporte</option>
                        <option value="permiso especial">Permiso Especial</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-sm-2 control-label">Numero Identificacion</label>
                <div class="col-sm-10">
                    <input type="text" value="{{ $empleado->identificacion ?? ''}}" class="form-control system_validador_vacio validarInputsTextIdentification" name="identificacion" id="identificacion" autocomplete="off" onpaste="return false">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-sm-2 control-label">Correo Electronico</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control system_validador_vacio" value="{{ $empleado->correo ?? ''}}" name="correo" id="correo" readonly>
                </div>
            </div>

            <input type="button" class="btn btn-primary" id="actualizarEmpleado" value="Actualizar">
        </form>
    </div>
</section>

<script>
    $("#actualizarEmpleado").on('click', function() {
        if (system_validarcampos("formEmpleado")) {
            axios.put('/platform/empleados/update', getDataJson("formEmpleado")).then(function(resp) {
                if (resp.data.error === 0) {
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'El empleado se ha modificado con exito!',
                        showConfirmButton: false,
                        timer: 1500
                    }).finally(() => {
                        window.location.href = "{{ url('platform/empleados') }}";
                    });
                } else {
                    notificarUsuario(resp.data.mensaje);
                }
            });
        }
    });

    function validarDatosParaCorreo() {
        $('#correo').val("");
        let nombre = $('#primer_nombre').val();
        let primer_apellido = $('#primer_apellido').val();
        let pais = $("#pais_empleo").val();

        if (nombre != "" && primer_apellido != "" && pais != "") {
            let primerNombre = nombre.split(" ").join('.');
            let apellidoSinEspacios = primer_apellido.split(" ").join(".");
            let correo = primerNombre + "." + apellidoSinEspacios + "@cidenet.com.";
            if (pais == "colombia") {
                correo += "co";
            } else if (pais == "estados unidos") {
                correo += "us";
            }

            $('#correo').val(correo);
        }
    }

    $('#primer_nombre').blur(function() {
        validarDatosParaCorreo();
    });

    $('#primer_apellido').blur(function() {
        validarDatosParaCorreo();
    });

    $('#pais_empleo').on('change', function() {
        let pais = $(this).val();

        if (pais != "") {
            validarDatosParaCorreo();
        }
    });
</script>
@endsection
