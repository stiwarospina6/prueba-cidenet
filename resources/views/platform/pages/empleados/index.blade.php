@extends('platform.layouts.app')
@section('title', 'Empleados')
@section('active-empleados', 'active')

@section('content')
<section class="card">
    <header class="card-header">
        <h2>Listado Empleados</h2>
        <div class="text-right ">
            <a href="{{ url('platform/empleados/create') }}" class="btn-sm btn-primary ">Crear</a>
        </div>
    </header>
    <div class="card-body">
        <div class="adv-table">
            <table class="display table table-bordered table-striped" id="listadoEmpleados">
                <thead>
                    <tr>
                        <th>Primer Nombre</th>
                        <th>Otros Nombres</th>
                        <th>Primer Apellido</th>
                        <th>Segundo Apellido</th>
                        <th>Tipo De Identificacion</th>
                        <th>Identificacion</th>
                        <th>Pais Empleo</th>
                        <th>Correo Electronico</th>
                        <th>Estado</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($empleados as $empleado)
                    <tr class="gradeX">
                        <td> {{ $empleado->primer_nombre }}</td>
                        <td> {{ $empleado->otros_nombres }}</td>
                        <td> {{ $empleado->primer_apellido }}</td>
                        <td> {{ $empleado->segundo_apellido }}</td>
                        <td> {{ $empleado->tipo_documento }}</td>
                        <td> {{ $empleado->identificacion }}</td>
                        <td> {{ $empleado->pais_empleo }}</td>
                        <td> {{ $empleado->correo }}</td>
                        <td>
                            <span class="badge badge-success">
                                {{ mb_strtoupper($empleado->estado) }}
                            </span>
                        </td>
                        <td class="text-center">
                            <a href="{{ url('platform/empleados/edit/'.$empleado->id) }}" class="badge badge-primary p-2"><i class="fa fa-edit"></i></a>
                            <a href="javascript:void(0)" data-id="{{ $empleado->id }}" class="badge badge-danger p-2 eliminarEmpleado">
                                <i class="fa fa-trash-o"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>

<script>
    $('.eliminarEmpleado').on('click', function() {
        let identificador = $(this).data("id");

        if (identificador != "") {
            Swal.fire({
                title: 'Esta seguro de eliminar el empleado?',
                showDenyButton: true,
                confirmButtonText: 'Si, estoy seguro!',
                denyButtonText: `Cancelar`,
            }).then((result) => {
                if (result.isConfirmed) {
                    eliminarEmpleado(identificador);
                }
            });
        } else {
            Swal.fire("Error!", "Ha surgido un error, por favor vuelva a intentarlo!", "error");
        }
    });

    function eliminarEmpleado(identificador) {
        axios({
            method: "delete",
            url: "empleados",
            data: {
                identificador: identificador
            }
        }).then(function(resp) {
            if (resp.data.error == 0) {
                Swal.fire("Exito!", "El empleado ha sido eliminado satisfactoriamente!", "success");

                setTimeout(function() {
                    location.reload();
                }, 2000);
            } else {
                Swal.fire("Error!", "Ha surgido un error, por favor vuelva a intentarlo!", "error");
            }
        });


    }
</script>
@endsection
