<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('./platform.includes.head')

<body>

    <section id="container" class="">
        <!--header start-->
        @include('./platform.includes.header')
        <!--header end-->
        <!--sidebar start-->
        @include('./platform.includes.menu')
        <!--sidebar end-->
        <!--main content start-->

        <section id="main-content">
            <section class="wrapper site-min-height">
                @yield('content')
            </section>
        </section>
        <!--main content end-->

        <!--footer start-->
        @include('./platform.includes.footer')
        <!--footer end-->
    </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="{{ asset('flatlab/js/bootstrap.bundle.min.js') }}"></script>
    <script class="include" type="text/javascript" src="{{ asset('flatlab/js/jquery.dcjqaccordion.2.7.js') }}">
    </script>
    <script src="{{ asset('flatlab/js/jquery.scrollTo.min.js') }}"></script>
    <script src="{{ asset('flatlab/js/jquery.nicescroll.js') }}" type="text/javascript"></script>
    <script src="{{ asset('flatlab/assets/advanced-datatable/media/js/jquery.dataTables.js') }}" type="text/javascript"></script>
    <script src="{{ asset('flatlab/assets/data-tables/DT_bootstrap.js') }}" type="text/javascript"></script>

    <script src="{{ asset('flatlab/js/respond.min.js') }}"></script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="{{ asset('js/app_request.js') }}"></script>
    <script src="{{ asset('js/validador.js') }}"></script>

    <!--right slidebar-->
    <script src="{{ asset('flatlab/js/slidebars.min.js') }}"></script>
    <script src="{{ asset('flatlab/js/dynamic_table_init.js') }}"></script>

    <!--common script for all pages-->
    <script src="{{ asset('flatlab/js/common-scripts.js') }}"></script>

    <script>
        $(document).ready(function() {
            let table = $('#listadoEmpleados').DataTable({
                language: {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        });
    </script>
</body>

</html>
