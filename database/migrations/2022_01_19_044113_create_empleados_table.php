<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpleadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleados', function (Blueprint $table) {
            $table->id();
            $table->string('primer_nombre', 20);
            $table->string('otros_nombres', 50)->nullable();
            $table->string('primer_apellido', 20);
            $table->string("segundo_apellido", 20);
            $table->string("pais_empleo");
            $table->string("tipo_documento");
            $table->string("identificacion", 20)->unique();
            $table->text("correo", 300)->unique();
            $table->string("area");
            $table->string("estado")->default("activo");
            $table->dateTime("fecha_registro", 1)->nullable();
            $table->dateTime("fecha_actualizacion", 1)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empleados');
    }
}
