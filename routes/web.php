<?php

use App\Http\Controllers\AutenticacionController;
use App\Http\Controllers\EmpleadosController;
use App\Http\Controllers\MasterController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AutenticacionController::class, 'login'])->name('login');

Route::post('/autenticacion/validar', [AutenticacionController::class, 'validar']);
Route::get('/autenticacion/salir', [AutenticacionController::class, 'logout']);

Route::group(['prefix' => 'platform', 'middleware' => 'auth'], function () {
    Route::get('dashboard', [MasterController::class, 'dashboard']);

    //empleados
    Route::get('empleados', [EmpleadosController::class, 'index']);
    Route::get('empleados/create', [EmpleadosController::class, 'create']);
    Route::post('empleados/store', [EmpleadosController::class, 'store']);
    Route::get('empleados/edit/{id}', [EmpleadosController::class, 'edit']);
    Route::put('empleados/update', [EmpleadosController::class, 'update']);
    Route::delete('empleados', [EmpleadosController::class, 'destroy']);
});
